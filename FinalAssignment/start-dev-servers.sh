#!/bin/bash

#########################################################################################
# Author: Sushan Chakraborty
# Date: 07/05/2019
# Instruction: initiate only from project's root directory, from command prompt/terminal
##########################################################################################


# #######################################################################################
readonly host_ip=0.0.0.0

readonly active_profile=dev

# Database configurations
# ########################################################################################
readonly MYSQL_DATABASE=assignment
readonly MYSQL_HOST=localhost
readonly MYSQL_PASSWORD=root
readonly MYSQL_USER=root
readonly MONGO_HOST=localhost
readonly HOST_NAME=localhost



function startMySqlAndMongoDockerContainer(){
	# down if there is any existing docker compose ps
	docker-compose -f docker-compose.yml down
	# Run mysql docker in detached mode( -d)
	docker-compose -f docker-compose.yml up -d
}

function setEnvironmentVariables(){

	if [[ -z "${ACTIVE_PROFILE}" ]]; then
		export ACTIVE_PROFILE=${active_profile}
		echo ${ACTIVE_PROFILE}
	fi

	if [[ -z "${MYSQL_HOST}" ]]; then
		export MYSQL_HOST=${MYSQL_HOST}
		echo ${MYSQL_HOST}
	fi

	if [[ -z "${MYSQL_DATABASE}" ]]; then
		export MYSQL_DATABASE=${MYSQL_DATABASE}
		echo ${MYSQL_DATABASE}
	fi

	if [[ -z "${MYSQL_USER}" ]]; then
		export MYSQL_USER=${MYSQL_USER}
		echo ${MYSQL_USER}
	fi

	if [[ -z "${MONGO_HOST}" ]]; then
		export MONGO_HOST=${MONGO_HOST}
		echo ${MONGO_HOST}
	fi


	if [[ -z "${MYSQL_PASSWORD}" ]]; then
		export MYSQL_PASSWORD=${MYSQL_PASSWORD}
		echo ${MYSQL_PASSWORD}
	fi

	if [[ -z "${HOST_IP}" ]]; then
		if [[ -z "${host_ip}" ]]; then
			echo "host ip not set, default is - localhost"
			export HOST_IP=localhost
		else
			export HOST_IP=${host_ip}
		fi
	fi


}

function startApiGatewayService(){
	# Start ApiGatewayService
	gnome-terminal -x sh -c "mvn spring-boot:run -f ApiGatewayService/pom.xml" 
}
function startCategoryService(){
	# Start CategoryService
	gnome-terminal -x sh -c "mvn spring-boot:run -f CategoryService/pom.xml -Dspring-boot.run.arguments=--MONGO_HOST=${MONGO_HOST}"
}
function startDiscoveryService(){
	# Start DiscoveryService
	gnome-terminal -x sh -c "mvn spring-boot:run -f DiscoveryService/pom.xml -Dspring-boot.run.arguments=--HOST_NAME=${HOST_NAME}" 
}
function startNoteService(){
	# Start NoteService
	gnome-terminal -x sh -c "mvn spring-boot:run -f NoteService/pom.xml -Dspring-boot.run.arguments=--MONGO_HOST=${MONGO_HOST}" 
}
function startReminderService(){
	# Start ReminderService
	gnome-terminal -x sh -c "mvn spring-boot:run -f ReminderService/pom.xml -Dspring-boot.run.arguments=--MONGO_HOST=${MONGO_HOST}" 
}
function startRibbonConfiguration(){
	# Start RibbonConfiguration
	gnome-terminal -x sh -c "mvn spring-boot:run -f RibbonConfiguration/pom.xml" 
}
function startUserAuthentication-Service(){
	# Start UserAuthentication-Service
	gnome-terminal -x sh -c "mvn spring-boot:run -f UserAuthentication-Service/pom.xml -Dspring-boot.run.arguments=--MYSQL_USER=${MYSQL_USER},--MYSQL_DATABASE=${MYSQL_DATABASE},--MYSQL_PASSWORD=${MYSQL_PASSWORD},--MYSQL_HOST=${MYSQL_HOST}" 
}



function startNoteUi(){
	# Start movie-cruiser-ui
	gnome-terminal -x \
	sh -c "echo \"starting frontend server...\" \
	&& cd note-ui && \
	(([ -d node_modules ] && ng serve --proxy-config proxy.conf.json --host ${HOST_IP} --open) \
	|| (npm install && ng serve --proxy-config proxy.conf.json --host ${HOST_IP} --open ))"
}

function checkSystem(){
	notready=false

	which docker &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker is installed..."
	else
		echo "docker is not installed, please install docker"
		notready=true
	fi

	which docker-compose &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker-compose is installed..."
	else
		echo "docker-compose is not installed, please install docker-compose"
		notready=true
	fi

	which mvn &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: maven is installed..."
	else
		echo "maven is not installed, please install maven"
		notready=true
	fi

	which node &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: node is installed..."
	else
		echo "node is not installed, please install nodejs"
		notready=true
	fi

	which npm &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: npm is installed..."
	else
		echo "npm is not installed, please install npm"
		notready=true
	fi

	which ng &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: angular-cli is installed..."
	else
		echo "angular-cli is not installed, please install angular-cli"
		notready=true
	fi

	if [[ notready == "true" ]]; then
		echo "Please install required software module(s) , then run script again"
		exit 1
	fi
}

function main(){
	checkSystem
	setEnvironmentVariables
	(startMySqlAndMongoDockerContainer || echo "Failed to start MySQL & Mongo docker container") \
	&& (startApiGatewayService || echo "Failed to start ApiGatewayService") \
	&& (startCategoryService || echo "Failed to start CategoryService") \
	&& (startDiscoveryService || echo "Failed to start DiscoveryService") \
	&& (startNoteService || echo "Failed to start NoteService") \
	&& (startReminderService || echo "Failed to start ReminderService") \
	&& (startRibbonConfiguration || echo "Failed to start RibbonConfiguration") \
	&& (startUserAuthentication-Service || echo "Failed to start UserAuthentication-Service") \
	&& (startNoteUi || echo "Failed to start NoteUi") \
	&& echo "note application started successfully at : http://${HOST_IP}:4200" \
	|| echo "Failed to start application!"
}

#main entry point of the program
main
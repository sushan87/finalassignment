package com.stackroute.keepnote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/*
 * The @SpringBootApplication annotation is equivalent to using @Configuration, @EnableAutoConfiguration 
 * and @ComponentScan with their default attributes
 */

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
public class CategoryServiceApplication {

	/*
	 * Define the bean for Filter registration. Create a new
	 * FilterRegistrationBean object and use setFilter() method to set new
	 * instance of JwtFilter object. Also specifies the Url patterns for
	 * registration bean.
	 */

	

	/*
	 * 
	 * You need to run SpringApplication.run, because this method start whole
	 * spring framework. Code below integrates your main() with SpringBoot
	 */

	public static void main(String[] args) {
		SpringApplication.run(CategoryServiceApplication.class, args);
	}
}

#!/bin/bash
#########################################################################################
# Author: Sushan Chakraborty 
# Date: 07/05/2019
# Instruction: initiate only from project's root directory, from command prompt/terminal
##########################################################################################




function checkSystem(){
	notready=false

	which docker &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker is installed..."
	else
		echo "docker is not installed, please install docker"
		notready=true
	fi

	which docker-compose &> /dev/null
	if [[ $? -eq 0 ]]; then
		echo "[OK]: docker-compose is installed..."
	else
		echo "docker-compose is not installed, please install docker-compose"
		notready=true
	fi

	if [[ notready == "true" ]]; then
		echo "Please install required software modules , then run script again"
		exit 1
	fi
}

function stop_and_remove_containers(){
	# down if there is any existing docker compose ps
	docker-compose down
}

function main(){
	checkSystem
	stop_and_remove_containers
}

#main entry point of the program
main
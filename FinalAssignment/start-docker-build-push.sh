#!/bin/bash

#########################################################################################
# Author: Sushan Chakraborty
# Date: 07/05/2019
# Instruction: initiate only from project's root directory, from command prompt/terminal
##########################################################################################

readonly DOCKHUB_ID=sushandocker
readonly REPO_NAME=noterepo
readonly TAG_NOTE_UI=note-ui
readonly TAG_APIGATEWAYSERVICE_API=ApiGatewayService
readonly TAG_CATEGORYSERVICE_API=CategoryService
readonly TAG_DISCOVERYERVICE_API=DiscoveryService
readonly TAG_NOTESERVICE_API=NoteService
readonly TAG_REMINDERSERVICE_API=ReminderService
readonly TAG_RIBBONCONFIGURATION_API=RibbonConfiguration
readonly TAG_USERAUTHENTICATIONSERVICE_API=UserAuthentication-Service



function build_docker_image(){
	local api_name=$1

	echo "starts building docker image of: ${api_name}..." \
	&& docker build -t "${DOCKHUB_ID}"/"${REPO_NAME}":"${api_name}" ./"${api_name}" \
	|| echo "[FAILED]:  building docker image of: ${api_name}..."
}

function build_api(){
	local api_name=$1

	echo "starts building ${api_name}..."
	if [[ "${TAG_NOTE_UI}" == "${api_name}" ]]; then
		npm --prefix ./"${api_name}" run build
	elif [ "${TAG_CATEGORYSERVICE_API}" == "${api_name}" ] || [ "${TAG_NOTESERVICE_API}" == "${api_name}" ] || [ "${TAG_REMINDERSERVICE_API}" == "${api_name}" ]; then
		mvn clean package -f ./"${api_name}"/pom.xml -DMONGO_HOST=localhost
	
	elif [[ "${TAG_USERAUTHENTICATIONSERVICE_API}" == "${api_name}" ]]; then
		cd ./"${api_name}"
		mvn clean package -f  pom.xml -DMYSQL_USER=root -DMYSQL_PASSWORD=root -DMYSQL_DATABASE=assignment -DMYSQL_HOST=localhost
		cd ..

	else 
		mvn clean package -f ./"${api_name}"/pom.xml
	fi
     

}

function build_api_docker_image(){
	local api_name=$1
	
	build_api "${api_name}"
	if [[ $? -eq 0 ]]; then
		echo "[SUCCESS]: building api: ${api_name}"
		build_docker_image "${api_name}"
	fi 
}

function remove_dangling_images(){
	echo "removing dangling docker images..."
	docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
}

function push_image(){
	local img_tag=$1
	docker push "${DOCKHUB_ID}"/"${REPO_NAME}":"${img_tag}"
}

# dockerhub login problem is the blocker for testing
function push_image_to_docker_hub(){
	
	docker login
	if [[ $? -eq 0 ]]; then
		(push_image "${TAG_APIGATEWAYSERVICE_API}" || echo "Failed to push image of ${TAG_APIGATEWAYSERVICE_API}") \
		&& (push_image "${TAG_CATEGORYSERVICE_API}" || echo "Failed to push image of ${TAG_CATEGORYSERVICE_API}") \
		&& (push_image "${TAG_DISCOVERYERVICE_API}" || echo "Failed to push image of ${TAG_DISCOVERYERVICE_API}") \
		&& (push_image "${TAG_NOTESERVICE_API}" || echo "Failed to push image of ${TAG_NOTESERVICE_API}") \
		&& (push_image "${TAG_REMINDERSERVICE_API}" || echo "Failed to push image of ${TAG_REMINDERSERVICE_API}") \
		&& (push_image "${TAG_RIBBONCONFIGURATION_API}" || echo "Failed to push image of ${TAG_RIBBONCONFIGURATION_API}") \
		&& (push_image "${TAG_USERAUTHENTICATIONSERVICE_API}" || echo "Failed to push image of ${TAG_USERAUTHENTICATIONSERVICE_API}") \
		&& (push_image "${TAG_NOTE_UI}" || echo "Failed to push image of ${TAG_NOTE_UI}") 
		docker logout
	else
		echo "docker hub login failed!..."
	fi
}

function main(){
	echo "This automation script is in progress"
	build_api_docker_image ${TAG_APIGATEWAYSERVICE_API}
	build_api_docker_image ${TAG_CATEGORYSERVICE_API}
	build_api_docker_image ${TAG_DISCOVERYERVICE_API}
	build_api_docker_image ${TAG_NOTESERVICE_API}
	build_api_docker_image ${TAG_REMINDERSERVICE_API}
	build_api_docker_image ${TAG_RIBBONCONFIGURATION_API}
	build_api_docker_image ${TAG_USERAUTHENTICATIONSERVICE_API}
	build_api_docker_image ${TAG_NOTE_UI}
	remove_dangling_images
	docker images

	push_image_to_docker_hub
}

#Program entry point
main

**Note taker App:**

A small application which helps its user to take notes of different categories & also to add reminders to each note. Each note will be of particular category. 
Multiple reminders can be tagged to a particular note.

## SCENE

| **SL#** | **SCENE**       |
| --------| --------------- |
| 1       | A user will have to login first or register  |
| 2       | A user can perform crud on notes. A note can be associated with a particular category & multiple reminders. Note has two views card & list. In list view notes are categorised based on their states not-started,started & completed. Category & Reminder have only card view|
| 3       | A user should be able to register him/herself to the by providing required details  |
| 4       | A user can perform crud on categories.   |
| 5       | A user can perform crud on reminders. |
| 6       | A uer should be able to logout from the site, only after successful login    |


## BEHIND THE SCENE

| **ACTORS** | **ROLE** | **STRENGTH** | 
| ----------| ------------ | --------------------------- | 
| note-ui | Microservice | Angular 5 |
| ApiGatewayService |  Microservice | Spring Boot, Zuul Gateway |
| DiscoveryService |  Microservice | Spring Boot,Netflix Eureka |
| RibbonConfiguration |  Microservice | Spring Boot, Netflix Ribbon for load balancing |
| NoteService |  Microservice | Spring Boot,JWT |
| CategoryService |  Microservice | Spring Boot,JWT |
| ReminderService |  Microservice | Spring Boot,JWT |
| UserAuthentication-Service| Micro service | Spring Boot, JWT |
| Container | Hosting and orchestration | Docer, Docker Compose |
| Datastore | MongoDB | NoSQL  | 
| Datastore | RDBMS | MySQL  | 


## Architecture diagram
![Architecture diagram](note-architecture.png)

## Swagger API Docs
![Category Service](Category-Service-swagger.png)
![Note Service](Note-Service-Swagger.png)
![Reminder Service](Reminder-Service-swagger.png)
![User Authentication Service](UserAuthemticationService-swagger.png)

## API status on Netflix Eureka
![eureka](eureka.png)





